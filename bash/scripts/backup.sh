#!/bin/bash

# Daily backup script

# Initialisation des variables
day=$(date +%F)
Folder="/home/$user/"
File="/home/backup_with_bash/$day.tar.gz"

# Sauvegarde du /home vers /backup_with_bash *
# (on adaptera sur un serveur distant pour plus d'efficacite)
tar cpzf $File $Folder

# On supprime les sauvegardes de plus de 6 mois
find $File* -mtime +180 -exec rm {} \;