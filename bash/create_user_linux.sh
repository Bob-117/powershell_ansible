#!/bin/bash

#title           :create_user_linux.sh
#description     :Script bash de creation d'utilisateurs a partir d'un fichier/ d'une liste
#                :Ajout d'un cron de sauvegarde quotidienne de /home
#author		     :bob117
#date            :20220524
#version         :1.0.0    
#usage		     :bash create_user_linux.sh
#notes           :v2.0.0 will need install jq
#bash_version    :4.4.23(2)-release

# À exécuter en tant que root

# On parcourt une liste donnee de nouveaux users
userfile=/userlist
username=$(cat userlist | tr 'A-Z' 'a-z')
password=$default_password

function create_user() {
    $password='default'
    pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
    now=$(date)
    adduser -e $now -p "$pass" "$1" # $1 = le premiere parametre lors de l'appel de la method (ligne 27)
}

# Si on est connecte en root
if [ $(id -u) -eq 0 ]; then
    for user in $username
    do
        # On verifie que le nouvel user n'existe pas
        egrep "^$user" /etc/passwd >/dev/null
        if [ $? -eq 0 ]; 
        then
            echo "$user exists!"
            exit 1
        else
        # Si il n'existe pas, on le creer avec un default password, a changer a la premiere connexion
        create_user $user
        # On verifie la creation
        [ $? -eq 0 ] && echo "User has been successfully added !" || echo "Failed to add new user!"

        # On lui ajoute un cron de savegarde quotidienne (toutes les nuits a 02h00)
        PATH=/bin:/usr/bin:/usr/local/bin
        sudo crontab -e
        0 2 * * * $user /scripts/backup.sh '$user'
        fi

    done
else
	echo "Only root may add a user to the system."
	exit 2
fi


# Ajout dynamique a des groupes existants :
<<COMMENT

# Utiliser jq pour lire le json
file : 
{
    'ADMIN' : {
        1: 'Bob',
        2: 'Gibson'
    },
    'Dev' : {
        1: 'Jimi',
        2: 'Angus'
    },
    'Exec' : {
        1: 'UnitTest',
        2: 'Python'
    }

}

# On considere que les groupes Admin, Dev et Exec existent deja

# On creer tous nos admin, on leur creer un /home et on les ajoute au groupe Admin
for new_user in $file | jq '.ADMIN'
do
    adduser -d $new_user
    usermod -a -G Admin $new_user
done

# On creer nos dev, on leur creer un /home si il n'existe pas
# on force le changement de password a la premiere connexion (-e)
for new_user in $file | jq '.Dev'
do
    adduser -d -e  $new_user
    usermod -a -G Dev $new_user
done

# On creer nos profils 'Exec', qui n'ont pas de session / home
# Ce sont des users systeme que l'ont utilise pour leurs roles/droits
# mais pas de session /home personnelle
for new_user in $file | jq '.Exec'
do
    useradd $new_user
    usermod -a -G Exec $new_user
done

COMMENT