<#
.Synopsis
   Script de sauvegarde du dossier utilisateur
   Backup script /home
.DESCRIPTION
   Script permettant la sauvegarde de documents
    Le cas d'un OS Windows est traite
    Les cas OS Linux et iOS ne sont pas encore implemente
.VERSION 
    1.0.0
.DATE
    2022.05.23
.AUTHOR
    Bob117
.NOTES
   Modifier la ligne 248 $path_from pour une utilisation plus generale
#>

Clear-Host
Write-Output "----------------------start----------------------"


<#
Recupere l'OS de la machine sur laquelle on execute le script
#>
function get_OS {
    #if ([System.Boolean](Get-CimInstance -ClassName Win32_OperatingSystem -ErrorAction SilentlyContinue)) # oof
    # Si l'OS en cours est un Windows
    IF ($env:OS -and $env:OS -like '*windows*')  #  IF ($isWindows)
    {
        RETURN 'windows'
    }
    # Sinon si l'OS est un linux
    ELSEIF ($PSVersionTable.OS -and $PSVersionTable.OS -like '*Linux*')
    {
        Write-Debug Linux
        # RETURN 'linux'
        Exit
    }
    elseif ($PSVersionTable.OS -and $PSVersionTable.OS -like '*Darwin*')
    {
        Write-Debug iOs
        # RETURN 'darwin'
        Exit
    }
    else { Exit }
}

<#
Creation d'un working_directory de test dans un $path defini
#>
function create_test_working_directory {
    [CmdletBinding()]
    param (
        [string] $path
    )
    <# 
    Creation d'une arborescence de test :
    /working_directory
    ___file_in_directory.txt
    ____/working_subdirectory
        ___file_in_subdirectory.txt
    #>
    Write-Verbose create_test_working_directory 
    New-Item -ItemType Directory -Force -Path $working_directory | Out-Null
    New-Item -ItemType File -Name "file_in_directory.txt" -Value "Im in the root directory" -Path $working_directory | Out-Null
    $working_subdirectory = $working_directory + '\working_subdirectory'
    New-Item -ItemType Directory -Force -Path $working_subdirectory | Out-Null
    New-Item -ItemType File -Name "file_in_subdirectory.txt" -Value "Im in the subdirectory" -Path $working_subdirectory | Out-Null

}

<#
Creation d'un working_directory de test dans un $OS defini
#>
function init_working_directory {
    [CmdletBinding()]
    param (
        [string] $OS
    )
    Write-Verbose init_working_directory
    switch ($OS) {
        'windows' {
            # mainpath = C:\Users\Bob
            $mainpath =  "{0}{1}" -F $env:HOMEDRIVE, $env:HOMEPATH 
            # Write-Debug $env:USERPROFILE

            # working_directory = C:\Users\Bob\workingdirectory
            $working_directory = $mainpath + "\working_directory_test"
            If(!(test-path $working_directory))
            {
                create_test_working_directory $working_directory
            }
            
            return $working_directory 
        }
        'linux' {
            <#
            working_directory = /home/Bob
            $working_directory = $env:HOME
            $mainpath = '/home/{0}/' -F $env:USERNAME
            mkdir
            #> 
            Write-Verbose "linux init_working_directory"
            Exit
        }
        'darwin' {
            <#
            working_directory = /Users/Bob
            create_test_working_directory $working_directory
            #>
            Write-Verbose "darwin init_working_directory"
            Exit
        }
    }
}

<#
Choix du device de destination de la sauvegarde
#>
function select_save_device_windows {
    # Instancation des choix du menu par defaut
    # $deviceC = New-Object System.Management.Automation.Host.ChoiceDescription '&C:/save_with_powershell', 'Save to device : C'
    # $deviceD = New-Object System.Management.Automation.Host.ChoiceDescription '&D:/save_with_powershell', 'Save to device : D'
    # $options = [System.Management.Automation.Host.ChoiceDescription[]]($deviceC, $deviceD)

    $title = 'Backup Save'
    $message = 'Where do u want to save ur files ?'
    # Nos choix par defaut sur windows
    $device_choices=@(
        ("&Quit","Quit"),
        ('&C:/save_with_powershell', 'Save to device : C'),
        ('&D:/save_with_powershell', 'Save to device : D')
    )
    $options = New-Object System.Collections.ObjectModel.Collection[System.Management.Automation.Host.ChoiceDescription]
    for($i=0; $i -lt $device_choices.length; $i++){
        $options.Add((New-Object System.Management.Automation.Host.ChoiceDescription $device_choices[$i] ) ) 
    }

    # Si une ou plusieurs cles usb sont detectees, on propose l'option "save to usb"
    if (Get-WmiObject win32_diskdrive | Where-Object{$_.Interfacetype -eq "USB"}) {
        $deviceUSB = New-Object System.Management.Automation.Host.ChoiceDescription '&USB', 'Save to device : USB'
        $options.Add($deviceUSB)
    }
    
    $result = $Host.ui.PromptForChoice($title, $message, $options, 0)

    switch ($result) {
        0 { Exit }
        1 { return 'C:' }
        2 { return 'D:' }
        3 { return 'USB' }
    }
} 

<#
Choix d'un usb device
#>
function select_usb_device {
    # dans le cas ou une ou plusieurs cles usb sont detectees
    $title = 'Available USB device'
    $message = 'Select ur destionation USB'
    $usb_choices=@(
        ("&Quit")
        #("&Cancel")
    )

    $usb_options = New-Object System.Collections.ObjectModel.Collection[System.Management.Automation.Host.ChoiceDescription]
    for($i=0; $i -lt $usb_choices.length; $i++){
        $usb_options.Add((New-Object System.Management.Automation.Host.ChoiceDescription $usb_choices[$i] ) ) 
    }
    
    # On recupere les lettres de lecteur
    $usb_letters = Get-WmiObject Win32_Volume -Filter "DriveType='2'" | Select-Object -expand driveletter
    
    # On cree une liste pour associer notre System.valueType (int) a la lettre usb associee
    $usb_SystemValueType_to_letter = @()
    
    # Pour chaque cle usb, on ajoute l'option dans le menu, et on met en place la correspondance { System.valueType $result : $lettre }
    foreach ($usb_letter in $usb_letters) {
        # $deviceUSB = New-Object System.Management.Automation.Host.ChoiceDescription ('&' + $usb_letter), ('Save to device : USB' + $usb_letter)
        $deviceUSB = New-Object System.Management.Automation.Host.ChoiceDescription::new('&' + $usb_letter)
        $usb_options.Add($deviceUSB)
        $usb_SystemValueType_to_letter += $usb_letter
    }
    $result = $Host.ui.PromptForChoice($title, $message, $usb_options, -1)

    if ($result -eq 0) { Exit }

    # Appel du menu principal si on souhaite annuler /!\ ca marche pas
    <# if ($result -eq 1){
        select_save_device_windows
    }#>

    # [0 : quit, 1 : E, 2 : F]
    $index = [int]$result - $usb_choices.Count
    return $usb_SystemValueType_to_letter[$index]     
}

<#
Sauvergarde de fichiers vers une destination selectionnee par l'utilisateur
#>
function save_to_device_windows {
    [CmdletBinding()]
    param (
        [string] $path_from,
        [string] $path_to
    )
    
    # On recupere la date pour garder un log / creer une save a la date/heure de l'execution du script
    
    # $current_date = Get-Date -Format "MM/dd/yyyy HH:mm:ss"
    # $save_directory = $current_date.Replace("/", "-").Replace(":", "-").Replace(" ", "_")
    $save_directory = Get-Date -Format 'yyyy-mm-dd_HH-mm-ss'

    # On cree notre directory de sauvegarde

    <#
    On s'assure de bien recuperer un path_to sous la forme 'lettre:\'
    Avec un magnifique ternaire dans un ternaire
    
    IF (path.len > 2)  { @({ nothing },{ add :\ })[path not end with :/] } ELSE { add :\ }
    #>

    $path_to_save = $( $( If ($path_to.length -gt 2) { @( ($path_to) , $($path_to+':\') )[ $path_to.Substring($path_to.length - 2, 2) -ne ':\'] } Else { $path_to[0]+':\' } ) + $( "{0}{1}{2}{3}" -F $env:USERNAME, "\saveWithPowerShell\", $(GET-DATE -Format 'yyyy-mm-dd_HH-mm-ss'), ".backup") )

    # Log info utilisateur
    $user_info = "You will save from {0} to {1}" -F $path_from, $path_to_save
    Write-Output $user_info
    Write-Output "Dont remove the device please :^)"
    Robocopy $path_from $path_to_save /s /njh /njs /ndl /nc /ns /nfl 
}

<#
Main function
#>
function main {
    # On recupere l'OS de la machine (et on suppose une architecture par defaut)
    $current_os = get_OS

    # On cree le directory de test pour eviter de cp mv tout le C:/user/$/documents
    <# 
    Dans un cas general:
    Switch ($OS)
        {
            'windows'{ $path_from = C:/Users/$env:USERNAME ou $path_from = $env:USERPROFILE }
            'linux -xor darwin' { $path_from = /home/$env:USERNAME }
        }
    #>
    $path_from = init_working_directory $current_os

    # Le cas windows est traite ici, on implemetera un switch case pour les cas linux et iOS
    $dest_device = select_save_device_windows
    if($dest_device -eq 'USB') 
    {
        $dest_device = select_usb_device
    }
    $path_to = $dest_device

    # On effectue la sauvegarde
    save_to_device_windows $path_from $path_to
}

 main

Write-Output "----------------------end----------------------"

#117