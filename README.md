# POEC CYBERSECURITE 2022 - EPSI Rennes

## Powershell backup  (default OS : windows)


- [x] default_save_with_powershell.ps1

```
----------------------start----------------------

Backup Save
Where do u want to save ur files ?
[Q] Quit  [C] C:/save_with_powershell  [D] D:/save_with_powershell  [U] USB  [?] Aide (la valeur par défaut est « Q ») : u

Available USB device
Select ur destionation USB
[Q] Quit  [E] E:  [F] F:  [?] Aide f
You will save from C:\Users\Bob\working_directory_test to F:\Bob\saveWithPowerShell\2022-53-22_01-53-56.backup
Dont remove the device please :^)

----------------------end----------------------
```

- [x] One-line zip archive on usb
```
$test = @(
        "x", 
        "xyz", 
        "x:\", 
        "xyz:\"
)

foreach ($path_to in $test) {
    echo ("-------------- " + $path_to + " --------------")
    echo $( $( If ($path_to.length -gt 2) { @( ($path_to) , $($path_to+':\') )[ $path_to.Substring($path_to.length - 2, 2) -ne ':\'] } Else { $path_to[0]+':\' } ) + $( "{0}{1}{2}{3}" -F $env:USERNAME, "\saveWithPowerShell\", $(GET-DATE -Format 'yyyy-mm-dd_HH-mm-ss'), ".backup") )

}

``` 
### Output :
```
-------------- x --------------
x:\Bob\saveWithPowerShell\2022-03-22_02-03-32.backup  
-------------- xyz --------------
xyz:\Bob\saveWithPowerShell\2022-03-22_02-03-32.backup
-------------- x:\ --------------
x:\Bob\saveWithPowerShell\2022-03-22_02-03-32.backup  
-------------- xyz:\ --------------
xyz:\Bob\saveWithPowerShell\2022-03-22_02-03-32.backup
```

### full one_line :
```
    # Select a device
    $path_to = [C, D, E, F] with E and F USB device

    Compress-Archive -Path $env:USERPROFILE -DestinationPath $( $( If ($path_to.length -gt 2) { @( ($path_to) , $($path_to+':\') )[ $path_to.Substring($path_to.length - 2, 2) -ne ':\'] } Else { $path_to[0]+':\' } ) + $( "{0}{1}{2}{3}" -F $env:USERNAME, "\saveWithPowerShell\", $(GET-DATE -Format 'yyyy-mm-dd_HH-mm-ss'), ".backup.zip") )


```

